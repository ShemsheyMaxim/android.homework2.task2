package com.maxim.task2;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView1, textView2, textView3, textView4;
    Random random = new Random();
    List<Integer> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView4 = (TextView) findViewById(R.id.textView4);

        textView1.setOnClickListener(this);
        textView2.setOnClickListener(this);
        textView3.setOnClickListener(this);
        textView4.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        list.add(getResources().getColor(R.color.colorRed));
        list.add(getResources().getColor(R.color.colorPink));
        list.add(getResources().getColor(R.color.colorPurple));
        list.add(getResources().getColor(R.color.colorIndigo));
        list.add(getResources().getColor(R.color.colorBlue));
        list.add(getResources().getColor(R.color.colorCyan));
        list.add(getResources().getColor(R.color.colorTeal));
        list.add(getResources().getColor(R.color.colorLime));
        list.add(getResources().getColor(R.color.colorYellow));
        list.add(getResources().getColor(R.color.colorDeepOrange));

        ColorDrawable viewColor = (ColorDrawable) v.getBackground();
        int colour = viewColor == null ? -1 : viewColor.getColor();
        int color1 = colour != -1 ? list.indexOf(colour) : random.nextInt(10);
        int color2 = random.nextInt(10);
        int color3 = random.nextInt(10);
        int color4 = random.nextInt(10);

        while (true) {
            if (color1 != color2) {
                break;
            } else {
                color2 = random.nextInt(10);
            }
        }
        while (true) {
            if (color3 != color2 && color3 != color1) {
                break;
            } else {
                color3 = random.nextInt(10);
            }
        }
        while (true) {
            if (color4 != color2 && color4 != color3 && color4 != color1) {
                break;
            } else {
                color4 = random.nextInt(10);
            }
        }
        if (v == textView1) {
            textView2.setBackgroundColor(list.get(color2));
            textView3.setBackgroundColor(list.get(color3));
            textView4.setBackgroundColor(list.get(color4));
        }
        if (v == textView2) {
            textView1.setBackgroundColor(list.get(color2));
            textView3.setBackgroundColor(list.get(color3));
            textView4.setBackgroundColor(list.get(color4));
        }
        if (v == textView3) {
            textView1.setBackgroundColor(list.get(color3));
            textView2.setBackgroundColor(list.get(color2));
            textView4.setBackgroundColor(list.get(color4));
        }
        if (v == textView4) {
            textView1.setBackgroundColor(list.get(color4));
            textView2.setBackgroundColor(list.get(color2));
            textView3.setBackgroundColor(list.get(color3));
        }
    }
}
